# Ansible role for authorized_key deployment

## Configuration
List of dicts `authorized_keys` with following structure
```
authorized_keys:
  - identifier: all
    entries:
      - user: root
        key:
          - key1
          - key2
          - key3
```

This role can be included as a dependency in other roles. Also usage from a playbook is possible with a composable list of ssh-keys. `authorized_keys` can be appended in different `group_vars` and `host_vars`.

Each entry will generate a yaml file `/etc/ansible/authorized_keys/` with the list of keys required for this `identifier`.
In a second step all of the files are ingested and sorted by user. All of the keys specified in the authorized_key files are deployed for each user.
